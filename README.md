secretsmanager
==============
The role manages secrets in AWS Secrets Manager. It runs AWS CLI commands on the local host

Requirements
------------
- User needs AWS secret keys

Role Variables
--------------
- command: list (default), describe, create, or update
- env: (required) sandbox, staging, or production
- service_name: (required) Name of Zift service requiring secrets

Dependencies
------------
None

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    secretsmanager.yml
    - hosts: localhost
      roles:
         - secretsmanager

    Examples:
    - List all secrets in Secrets Manager
    $ ansible-playbook -i "localhost," secretsmanager.yml

    - Describe the sandbox-opportunities-service secret
    $ ansible-playbook -i "localhost," secretsmanager.yml \
      -e "command=describe \
          env=sandbox \
          service_name=opportunities-service"

    - Create the sandbox-opportunities-service secret
    $ ansible-playbook -i "localhost," secretsmanager.yml \
      -e "command=create \
          env=sandbox \
          service_name=opportunities-service"

    - Update the sandbox-opportunities-service secret
    $ ansible-playbook -i "localhost," secretsmanager.yml \
      -e "command=update \
          env=sandbox \
          service_name=opportunities-service"

    NOTE: For create and update command the corresponding
    (env)-(service_name).json must exist in the files
    directory of the secretsmanager role
License
-------
Zift Solutions

Author Information
------------------
Chris Fouts
